package com.example.flashbytes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class PhoneNumberPage extends AppCompatActivity {
    private EditText phoneNumber;
    private String number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phone_number);

        phoneNumber =findViewById(R.id.editText);


    }

    public void verifyOtpPage(View view) {
        number = phoneNumber.getText().toString();
        Intent i = new Intent(PhoneNumberPage.this, OTPVerify.class);
        i.putExtra("EXTRA_SESSION_ID", number);
        startActivity(i);
    }
}