package com.example.flashbytes;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.android.gms.tasks.OnCompleteListener;

import java.util.concurrent.TimeUnit;

public class OTPVerify extends AppCompatActivity {

    private TextView nameText;
    private EditText otpText;

    FirebaseAuth auth;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback;
    private String verificationCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpverify);


        String phoneNumber = getIntent().getStringExtra("EXTRA_SESSION_ID");

        nameText =findViewById(R.id.textView3);
        nameText.setText("+91 " + phoneNumber);


        StartFirebaseLogin();

        sendOtp();

    }

    private void sendOtp() {
        String num = getIntent().getStringExtra("EXTRA_SESSION_ID");

        String phoneNumber = "+91" + num;

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,                     // Phone number to verify
                60,                           // Timeout duration
                TimeUnit.SECONDS,                // Unit of timeout
                OTPVerify.this,        // Activity (for callback binding)
                mCallback);                      // OnVerificationStateChangedCallbacks
    }



    public void verifyOtp(View view) {

        otpText = findViewById(R.id.editTextNumberPassword);
        String otp = otpText.getText().toString();

        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationCode, otp);

        SigninWithPhone(credential);

    }

    private void SigninWithPhone(PhoneAuthCredential credential) {
        auth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            startActivity(new Intent(OTPVerify.this, HomePage.class));
                            finish();
                        } else {
                            Toast.makeText(OTPVerify.this,"Incorrect OTP",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void StartFirebaseLogin() {

        auth = FirebaseAuth.getInstance();
        mCallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                Toast.makeText(OTPVerify.this,"verification completed",Toast.LENGTH_SHORT).show();

                Log.d("OTP SEND", String.valueOf(phoneAuthCredential));
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Toast.makeText(OTPVerify.this,"verification failed",Toast.LENGTH_SHORT).show();

                Log.d("Hello", String.valueOf(e));
            }

            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                verificationCode = s;
                Toast.makeText(OTPVerify.this,"Code sent",Toast.LENGTH_SHORT).show();
            }
        };
    }

    public void sendOtpAgain(View view) {
        sendOtp();
    }
}